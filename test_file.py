# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=line-too-long

"""Модуль для тестирования основного функционала
"""

import contextlib
import io
import os
import tempfile
import unittest

import control_unit
import translator
from control_unit import ControlUnit
from datapath import DataPath
from tools import Signal, HaltProgram, SegmentOverflow


class TestTranslator(unittest.TestCase):

    def test_euler(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            source = "programs/euler.forth"
            target = os.path.join(tmpdirname, "euler.out")
            input_stream = "in/euler.txt"

            with contextlib.redirect_stdout(io.StringIO()) as stdout:
                translator.main([source, target])
                control_unit.main([target, input_stream])

            self.assertEqual(stdout.getvalue(),
                             '232792560 \nticks: 574\n')

    def test_hello(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            source = "programs/print_hello.forth"
            target = os.path.join(tmpdirname, "print_hello.out")

            with contextlib.redirect_stdout(io.StringIO()) as stdout:
                translator.main([source, target])
                control_unit.main([target])

            self.assertEqual(stdout.getvalue(), 'Hello World! \nticks: 6\n')

    def test_cat(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            source = "programs/cat.forth"
            target = os.path.join(tmpdirname, "cat.out")
            input_stream = "in/cat.txt"

            with contextlib.redirect_stdout(io.StringIO()) as stdout:
                translator.main([source, target])
                control_unit.main([target, input_stream])

            self.assertEqual(stdout.getvalue(), 'foo \nticks: 11\n')

    def test_cat_empty(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            source = "programs/cat.forth"
            target = os.path.join(tmpdirname, "cat.out")

            with contextlib.redirect_stdout(io.StringIO()) as stdout:
                translator.main([source, target])
                control_unit.main([target])

            self.assertEqual(stdout.getvalue(), '\nticks: 7\n')

    def test_translator_euler(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            source = "programs/euler.forth"
            target = os.path.join(tmpdirname, "euler.out")
            translator.main([source, target])
            with open(target, encoding="utf-8") as file:
                content = file.read()
            self.assertEqual(content,
                             """0000000100 ztop
0100000101 jz 5
0000000011 swap
0000000010 %
0110000000 jmp 0
0000000001 +
0000001001 ret
0000000111 otop
0100001011 jz 11
0000001000 <<
0110000111 jmp 7
0000000101 last
0100010010 jz 18
0000001010 dup_sec
0000001010 dup_sec
0010000000 call 0
0000001011 lcm
0110001011 jmp 11
0000000110 .
0000000000 halt
""")

    def test_translator_print(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            source = "programs/print_hello.forth"
            target = os.path.join(tmpdirname, "hello.out")
            translator.main([source, target])
            with open(target, encoding="utf-8") as file:
                content = file.read()
            self.assertEqual(content,
                             """1000000000 < Hello
1000000001 < World!
0000000000 halt
""")

    def test_translator_cat(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            source = "programs/cat.forth"
            target = os.path.join(tmpdirname, "cat.out")
            translator.main([source, target])
            with open(target, encoding="utf-8") as file:
                content = file.read()
            self.assertEqual(content,
                             """0000000100 ztop
0100000100 jz 4
0000001000 <<
0110000000 jmp 0
0000000110 .
0000000000 halt
""")

    def test_reading_instructions_euler(self):
        bin_code_file = "out/euler.out"
        instructions, start_address, arg_words = translator.read_instructions(bin_code_file)
        expected_instructions = ['0000000100',
                                 '0100000101',
                                 '0000000011',
                                 '0000000010',
                                 '0110000000',
                                 '0000000001',
                                 '0000001001',
                                 '0000000111',
                                 '0100001011',
                                 '0000001000',
                                 '0110000111',
                                 '0000000101',
                                 '0100010010',
                                 '0000001010',
                                 '0000001010',
                                 '0010000000',
                                 '0000001011',
                                 '0110001011',
                                 '0000000110',
                                 '0000000000']
        exp_start_address = 7
        exp_arg_words = []
        self.assertEqual(instructions, expected_instructions)
        self.assertEqual(start_address, exp_start_address)
        self.assertEqual(arg_words, exp_arg_words)

    def test_reading_instructions_hello(self):
        bin_code_file = "out/print_hello.out"
        instructions, start_address, arg_words = translator.read_instructions(bin_code_file)
        expected_instructions = ['1000000000', '1000000001', '0000000000']
        exp_start_address = 0
        exp_arg_words = ['Hello', 'World!']
        self.assertEqual(instructions, expected_instructions)
        self.assertEqual(start_address, exp_start_address)
        self.assertEqual(arg_words, exp_arg_words)

    def test_reading_instructions_cat(self):
        bin_code_file = "out/cat_hello.out"
        instructions, start_address, arg_words = translator.read_instructions(bin_code_file)
        expected_instructions = ['0000000100',
                                 '0100000100',
                                 '0000001000',
                                 '0110000000',
                                 '0000001100',
                                 '0000000000']
        exp_start_address = 0
        exp_arg_words = []
        self.assertEqual(instructions, expected_instructions)
        self.assertEqual(start_address, exp_start_address)
        self.assertEqual(arg_words, exp_arg_words)

    def test_fill_memory(self):
        instructions = ['1000000000', '1000000001', '0000000000']
        start_address = 0
        arg_words = ['Hello', 'World!']
        input_tokens = []
        data_path = DataPath(input_tokens, instructions, start_address, arg_words)

        exp_memory = instructions + [0] * (200 - len(instructions))
        exp_memory[100] = 'Hello'
        exp_memory[101] = 'World!'
        self.assertEqual(data_path.memory, exp_memory)

        data_path.stack.append(0)
        data_path.stack_pointer += 1
        data_path.handle_signal(Signal.WRITE_ARG)
        self.assertEqual(data_path.stack, [])
        data_path.stack.append(1)
        data_path.stack_pointer += 1
        data_path.handle_signal(Signal.WRITE_ARG)
        self.assertEqual(data_path.stack, [])

        self.assertEqual(data_path.memory, exp_memory)
        self.assertEqual(data_path.output_tokens, ['H', 'e', 'l', 'l', 'o', ' ', 'W', 'o', 'r', 'l', 'd', '!', ' '])

    def test_segmentation_overflow(self):
        swap_instruction = ['0000000011']
        instructions = swap_instruction * 100
        start_address = 0
        arg_words = ['Hello', 'World!']
        input_tokens = ['']
        data_path = DataPath(input_tokens, instructions, start_address, arg_words)
        data_path.stack = ['1', '2', '3']
        my_control_unit = ControlUnit(data_path)
        my_control_unit.start()
        self.assertRaises(SegmentOverflow)
        self.assertRaises(HaltProgram)

    def test_lcm(self):
        data_path = DataPath([], [], 0, [])
        data_path.stack = ['2', '17', '1']
        data_path.handle_signal(Signal.LCM)
        self.assertEqual(int(data_path.stack[0]), 34)

        data_path.stack = ['16', '8', '8']
        data_path.handle_signal(Signal.LCM)
        self.assertEqual(int(data_path.stack[0]), 16)

        data_path.stack = ['2017', '51', '1']
        data_path.handle_signal(Signal.LCM)
        self.assertEqual(int(data_path.stack[0]), 102867)

    def test_add_mod(self):
        data_path = DataPath([], [], 0, [])
        data_path.stack = ['5', '10']
        data_path.handle_signal(Signal.ADD)
        self.assertEqual(int(data_path.stack[0]), 15)

        data_path.stack = ['-10', '10']
        data_path.handle_signal(Signal.ADD)
        self.assertEqual(int(data_path.stack[0]), 0)

        data_path.stack = ['16', '4']
        data_path.handle_signal(Signal.MOD)
        self.assertEqual(int(data_path.stack[0]), 0)

        data_path.stack = ['4', '16']
        data_path.handle_signal(Signal.MOD)
        self.assertEqual(int(data_path.stack[0]), 4)

    def test_swap(self):
        data_path = DataPath([], [], 0, [])
        data_path.stack = ['5', '10']
        data_path.handle_signal(Signal.SWAP)
        self.assertEqual(data_path.stack, ['10', '5', '10'])

    def test_dup(self):
        data_path = DataPath([], [], 0, [])
        data_path.stack = ['5', '10']
        data_path.stack_pointer = 1
        data_path.handle_signal(Signal.DUPLICATE_SECOND)
        self.assertEqual(data_path.stack, ['5', '10', '5'])

        data_path.stack = ['10']
        data_path.stack_pointer = 0
        data_path.handle_signal(Signal.DUPLICATE_SECOND)
        self.assertEqual(data_path.stack, ['10'])

    def test_check_not_zero(self):
        data_path = DataPath([], [], 0, [])
        data_path.stack = ['5', '10', '0']
        data_path.stack_pointer = 2
        data_path.handle_signal(Signal.CHECK_NOT_ZERO)
        self.assertEqual(data_path.stack[data_path.stack_pointer], 0)

        data_path.stack = ['5', '10', 0]
        data_path.stack_pointer = 2
        data_path.handle_signal(Signal.CHECK_NOT_ZERO)
        self.assertEqual(data_path.stack[data_path.stack_pointer], 0)

        data_path.stack = ['5', '10']
        data_path.stack_pointer = 1
        data_path.handle_signal(Signal.CHECK_NOT_ZERO)
        self.assertEqual(data_path.stack[data_path.stack_pointer], 1)

        data_path.stack = []
        data_path.stack_pointer = -1
        data_path.handle_signal(Signal.CHECK_NOT_ZERO)
        self.assertEqual(data_path.stack[data_path.stack_pointer], 1)

        data_path.stack = ['5', '10', 'Hello']
        data_path.stack_pointer = 2
        data_path.handle_signal(Signal.CHECK_NOT_ZERO)
        self.assertEqual(data_path.stack[data_path.stack_pointer], 1)

    def test_not_last(self):
        data_path = DataPath([], [], 0, [])
        data_path.stack = ['5', '10', '0']
        data_path.handle_signal(Signal.CHECK_NOT_LAST)
        self.assertEqual(data_path.stack[3], 1)

        data_path.stack = ['Hi']
        data_path.handle_signal(Signal.CHECK_NOT_LAST)
        self.assertEqual(data_path.stack[1], 0)

    def test_one(self):
        data_path = DataPath([], [], 0, [])
        data_path.stack = ['5', '10', '1']
        data_path.stack_pointer = 2
        data_path.handle_signal(Signal.CHECK_ONE)
        self.assertEqual(data_path.stack[3], 0)

        data_path.stack = ['5', '10', 1]
        data_path.stack_pointer = 2
        data_path.handle_signal(Signal.CHECK_ONE)
        self.assertEqual(data_path.stack[3], 0)

        data_path.stack = ['5', '10', '1Hello']
        data_path.stack_pointer = 2
        data_path.handle_signal(Signal.CHECK_ONE)
        self.assertEqual(data_path.stack[3], 1)

    def test_read_write(self):
        data_path = DataPath([], [], 0, [])

        data_path.input_tokens = ['A', 'n', 'a']
        data_path.handle_signal(Signal.WRITE)
        self.assertEqual(data_path.stack, ['Ana'])

        data_path.input_tokens = ['A', 'n', 'a', ' ', 'A']
        data_path.stack = []
        data_path.handle_signal(Signal.WRITE)
        self.assertEqual(data_path.stack, ['Ana'])
        data_path.handle_signal(Signal.WRITE)
        self.assertEqual(data_path.stack, ['Ana', 'A'])

        data_path.input_tokens = ['A', 'n', 'a', ' ', 'A', '1', '2', ' ', '9']
        data_path.stack = []
        data_path.handle_signal(Signal.WRITE)
        data_path.handle_signal(Signal.WRITE)
        data_path.handle_signal(Signal.WRITE)
        self.assertEqual(data_path.stack, ['Ana', 'A12', '9'])

        data_path.handle_signal(Signal.READ)
        self.assertEqual(data_path.stack, ['Ana', 'A12'])
        self.assertEqual(data_path.output_tokens, ['9', ' '])

        data_path.handle_signal(Signal.READ)
        self.assertEqual(data_path.stack, ['Ana'])
        self.assertEqual(data_path.output_tokens, ['9', ' ', 'A', '1', '2', ' '])
